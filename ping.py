import subprocess
import os
with open(os.devnull, "wb") as limbo:
        for n in range(1, 255):
                ip="10.0.0.{0}".format(n)
                result=subprocess.Popen(["ping", "-n", "1", "-w", "1", ip],
                        stdout=limbo, stderr=limbo).wait()
                if result:
                        print (ip + " Inactive")
                else:
                        print (ip + " Active")
